var StudentsAndPoints = ['Алексей Петров', 0,
						'Ирина Овчинникова', 60,
						'Глеб Стукалов', 30, 
						'Антон Павлович', 30,
						'Виктория Заровская', 30,
						'Алексей Левенец', 70,
						'Тимур Вамуш', 30,
						'Евгений Прочан', 60,
						'Александр Малов', 0];

	
var students = StudentsAndPoints.filter(function(item){
  return typeof(item) === "string";
});
var points = StudentsAndPoints.filter(function(item){
  return typeof(item) === "number";
});

console.log('Список студентов:')
students.forEach(function (name, i) {
console.log('Студент ' + name + ' набрал ' + points[i] + ' баллов');
});


var max, maxIndex;
points.forEach(function (number, i) {
if (!max || number > max) {
max = number;
maxIndex = i;
}
});
console.log('Студент набравший максимальный балл: ' + students[maxIndex] + ' (' + max + ' баллов)')


console.log('Список студентов после получения дополнительных баллов:')
points = points.map ( function (point, i) {
  if ( students[i] === 'Ирина Овчинникова' || students[i] === 'Александр Малов' ) { 
    return point += 30; 
 }
  else {
    return point;
  }
     
});
students.forEach(function (name, i) {
console.log('Студент ' + name + ' набрал ' + points[i] + ' баллов');
});

var pointsNew = points.slice();
var studentsNew = students.slice();
var counter = 0;
var newTop = [];

function getTop(numTop){
  if(numTop > points.length){
    numTop = points.length; 
  };
  if(counter < numTop){
    var max, maxIndex;
    pointsNew.forEach(function(point, i){
    if(!max || point > max){
        max = point;
        maxIndex = i;
     }
    });
    newTop.push(studentsNew[maxIndex]);
    newTop.push(max);
    studentsNew.splice(maxIndex, 1);
    pointsNew.splice(maxIndex, 1);
    counter++;
    getTop(numTop);
  } else{
    counter = 0;
    pointsNew = points.slice();
    studentsNew = students.slice();
    Top = newTop.slice(); 
    newTop = [];
  };
   return Top;
};
getTop(3);
getTop(5);

console.log('ТОП-3:\n' + getTop(3));
console.log('ТОП-5:\n' + getTop(5));



